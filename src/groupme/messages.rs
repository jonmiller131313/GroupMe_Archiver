use anyhow::{Context, Result};
use lazy_static::lazy_static;
use reqwest::blocking::Client;
use std::string::ToString;

use crate::db::RowID;
use crate::groupme::api::*;

pub const GROUPME_MESSAGE_PAGE_SIZE_INT: usize = 100;
lazy_static! {
    static ref GROUPME_MESSAGE_PAGE_SIZE_STR: String = GROUPME_MESSAGE_PAGE_SIZE_INT.to_string();
}

pub struct MessagePages<'a> {
    client: &'a Client,
    chat_id: RowID,
    last_message_id: RowID,
    num_messages: Option<usize>,
    message_count: usize,
}

impl<'a> MessagePages<'a> {
    pub fn new(client: &'a Client, chat_id: RowID) -> Self {
        Self {
            client,
            chat_id,
            last_message_id: 0,
            num_messages: None,
            message_count: 0,
        }
    }
}

impl Iterator for MessagePages<'_> {
    type Item = Result<Vec<Message>>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.num_messages.is_some() && self.message_count >= self.num_messages.unwrap() {
            return None;
        }

        let message_meta: MessagesDetail = match send_request(
            &self.client,
            BASE_URL
                .join("groups/")
                .expect("Hardcoded URL 1")
                .join(&format!("{}/", self.chat_id))
                .expect("Converting unsigned to string shouldn't fail")
                .join("messages")
                .expect("Hardcoded URL 2"),
            &[
                ("limit", &GROUPME_MESSAGE_PAGE_SIZE_STR),
                ("after_id", &self.last_message_id.to_string()),
            ],
        )
        .context("Unable to get request for messages")
        {
            Err(e) => return Some(Err(e)),
            Ok(m) => m,
        };
        if message_meta.messages.is_empty() {
            return None;
        }

        self.message_count += message_meta.messages.len();
        if self.num_messages.is_none() {
            self.num_messages = Some(message_meta.count);
        }
        self.last_message_id = message_meta.messages.last().unwrap().id.parse().unwrap();

        return Some(Ok(message_meta.messages));
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        lazy_static! {
            static ref PAGE_SIZE: f64 = GROUPME_MESSAGE_PAGE_SIZE_STR
                .parse()
                .expect("Hardcoded integer");
        }
        let num_pages_left = self.num_messages.map(|num_total| {
            ((num_total - self.message_count) as f64 / *PAGE_SIZE).ceil() as usize
        });

        (num_pages_left.unwrap_or(0), num_pages_left)
    }
}
