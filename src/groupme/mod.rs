use std::{
    collections::HashMap,
    fmt::{format, Write as FmtWrite},
    fs::{create_dir_all, File},
    io::{BufRead, BufReader, Write as IoWrite},
    path::{Path, PathBuf},
};

use anyhow::{ensure, Context, Error, Result};
use indicatif::*;
use lazy_static::lazy_static;
use rayon::prelude::*;
use reqwest::{
    blocking::Client,
    header::{HeaderMap, HeaderName, HeaderValue},
};
use url::Url;

pub mod api;
mod messages;

use crate::db::RowID;
use api::*;
use messages::*;

pub struct Token(HeaderValue);

impl Token {
    pub fn read(token_file: &Path, prompt: bool) -> Result<Self> {
        lazy_static! {
            static ref GROUPME_API_URL: Url =
                Url::parse("https://dev.groupme.com/").expect("Hardcoded URL");
        }
        if prompt && token_file.as_os_str() == "/dev/stdin" {
            eprintln!("You can get your API token here: {}", *GROUPME_API_URL);
            eprint!("API Token: ");
        }

        Ok(Self(
            HeaderValue::from_str(
                BufReader::new(
                    File::open(token_file)
                        .with_context(|| format!("Error opening file: {token_file:?}"))?,
                )
                .lines()
                .next()
                .context("No text")?
                .context("No valid UTF-8")
                .and_then(|str| {
                    ensure!(str.len() > 0, "First line is empty");
                    Ok(str)
                })?
                .as_str(),
            )
            .context("Token is not visible ASCII")?,
        ))
    }
}

pub trait DownloadFromGroupMe {
    fn get_groups(&self) -> Result<Vec<ChatDetail>>;
    fn get_all_messages(
        &self,
        group_info: &Vec<(RowID, String, usize)>,
        print_progress: bool,
    ) -> Result<Vec<Vec<Message>>>;
}

impl TryFrom<Token> for Client {
    type Error = Error;

    fn try_from(token: Token) -> Result<Self> {
        Ok(Client::builder()
            .default_headers(
                (&HashMap::from([("x-access-token".to_string(), token.0)]))
                    .try_into()
                    .unwrap(),
            )
            .build()
            .context("Unable to build HTTP client")?)
    }
}

impl DownloadFromGroupMe for Client {
    fn get_groups(&self) -> Result<Vec<ChatDetail>> {
        send_request(
            self,
            BASE_URL.join("groups").expect("Hard-coded URL"),
            &[("per_page", "500")],
        )
    }

    fn get_all_messages(
        &self,
        group_info: &Vec<(RowID, String, usize)>,
        print_progress: bool,
    ) -> Result<Vec<Vec<Message>>> {
        create_dir_all("messages").context("Unable to create messages directory")?;

        let multi_progress = MultiProgress::new();
        let progress_bar = multi_progress.add(if print_progress {
            ProgressBar::new(group_info.len() as u64)
        } else {
            ProgressBar::hidden()
        });
        let progress_style = ProgressStyle::with_template("{prefix} {pos:>3}/{len:3} [{wide_bar}]")
            .expect("Hardcoded template")
            .progress_chars("#>-");
        progress_bar.set_style(progress_style);
        progress_bar.set_prefix("Downloading messages:");

        let messages = group_info
            .par_iter()
            .map(|(id, name, num)| {
                download_chat_messages(
                    &self,
                    id,
                    name,
                    num,
                    print_progress.then_some(&multi_progress),
                )
            })
            .inspect(|_| progress_bar.inc(1))
            .collect::<Result<Vec<_>, _>>()
            .context("Error while running message downloading workers")?;
        progress_bar.finish_and_clear();

        return Ok(messages);
    }
}

fn download_chat_messages(
    client: &Client,
    chat_id: &RowID,
    chat_name: &str,
    num_messages: &usize,
    multi_progress: Option<&MultiProgress>,
) -> Result<Vec<Message>> {
    let filename = PathBuf::from(format!("messages/{}.txt", chat_name.replace("/", "\\")));
    let mut file =
        File::create(&filename).with_context(|| format!("Unable to open file: {filename:?}"))?;

    let progress_bar = if let Some(multi_progress) = multi_progress {
        multi_progress.add(ProgressBar::new(*num_messages as u64))
    } else {
        ProgressBar::hidden()
    };
    let progress_style = ProgressStyle::with_template(
        "{spinner} ({eta:3}) {pos:>6}/{len:6} <{prefix}> [{wide_bar}]",
    )
    .expect("Hardcoded style")
    .progress_chars("#>-");
    progress_bar.set_style(progress_style);
    progress_bar.set_prefix(chat_name.to_string());

    let mut messages = Vec::with_capacity(*num_messages);
    for (i, loaded_messages) in MessagePages::new(client, *chat_id)
        .enumerate()
        .inspect(|(i, _)| {
            progress_bar.inc(1 * GROUPME_MESSAGE_PAGE_SIZE_INT as u64);
        })
    {
        let mut loaded_messages = loaded_messages.with_context(|| {
            format!(
                "Unable to load messages (page #{}/{}) from chat: {chat_name}",
                i + 1,
                (*num_messages as f64 / GROUPME_MESSAGE_PAGE_SIZE_INT as f64).ceil() as usize
            )
        })?;

        loaded_messages
            .iter()
            .map(|message| writeln!(&mut file, "{message:?}"))
            .collect::<Result<_, _>>()
            .with_context(|| format!("Error while writing file: {filename:?}"))?;

        messages.append(&mut loaded_messages);
    }

    return Ok(messages);
}
