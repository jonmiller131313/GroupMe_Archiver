use std::collections::HashMap;

use anyhow::{Context, Error, Result};
use lazy_static::lazy_static;
use reqwest::blocking::{Client, RequestBuilder};
use serde::Deserialize;
use serde_json::{self as json, value::Value as JsonValue};
use url::Url;

lazy_static! {
    pub static ref BASE_URL: Url =
        Url::parse("https://api.groupme.com/v3/").expect("Hardcoded URL");
}

#[derive(Deserialize, Debug, Clone)]
pub struct GroupMeResponse {
    pub response: JsonValue,
    pub meta: GroupMeResponseMeta,
}

#[derive(Deserialize, Debug, Clone)]
pub struct GroupMeResponseMeta {
    pub code: usize,
    pub errors: Option<Vec<String>>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct ChatDetail {
    pub id: String,
    pub name: String,
    #[serde(rename(deserialize = "type"))]
    pub type_: String,
    pub description: String,
    pub image_url: Option<Url>,
    pub creator_user_id: String,
    pub created_at: usize,
    pub updated_at: usize,
    pub members: Vec<UserDetail>,
    pub share_url: Option<Url>,
    pub messages: ChatDetailMessagesMeta,
}

#[derive(Deserialize, Debug, Clone)]
pub struct UserDetail {
    pub user_id: String,
    pub nickname: String,
    pub muted: bool,
    pub image_url: String, // Allows "" as a url so we have to use String
}

#[derive(Deserialize, Debug, Clone)]
pub struct ChatDetailMessagesMeta {
    pub count: usize,
    pub last_message_id: String,
    pub last_message_created_at: usize,
    pub preview: ChatDetailMessagesPreview,
}

#[derive(Deserialize, Debug, Clone)]
pub struct ChatDetailMessagesPreview {
    pub nickname: String,
    pub text: String,
    pub image_url: String,
    pub attachments: Vec<MessageAttachment>, // Polymorphic objects dictated by "type" property
}

#[derive(Deserialize, Debug, Clone)]
pub struct MessagesDetail {
    pub count: usize,
    pub messages: Vec<Message>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Message {
    pub id: String,
    pub source_guid: String,
    pub created_at: usize,
    pub user_id: String,
    pub group_id: String,
    pub name: String,
    pub avatar_url: Option<String>,
    pub text: Option<String>,
    pub system: bool,
    pub favorited_by: Vec<String>,
    pub attachments: Vec<MessageAttachment>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct MessageAttachment {
    #[serde(rename(deserialize = "type"))]
    pub type_: String,

    #[serde(flatten)]
    pub data: HashMap<String, JsonValue>,
}

pub fn send_request<T>(client: &Client, url: Url, params: &[(&str, &str)]) -> Result<T>
where
    T: serde::de::DeserializeOwned,
{
    json::from_value(
        client
            .get(url)
            .query(params)
            .send()
            .context("Unable to send request")?
            .error_for_status()
            .map_err(|e| Error::msg(format!("Server responded with {e}")))?
            .json::<GroupMeResponse>()
            .context("Unable to parse top level JSON response")?
            .response,
    )
    .context("Unable to parse JSON response")
}
