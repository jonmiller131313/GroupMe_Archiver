use std::convert::From;

use anyhow::{Context, Result};
use bytes::Bytes;
use chrono::{offset::TimeZone, DateTime, NaiveDateTime, Utc};
use lazy_static::lazy_static;
use regex::Regex;
use rusqlite as db;
use url::Url;

use crate::db::RowID;
use crate::groupme::api;

pub mod insert;

#[derive(Clone, Debug)]
pub struct Chat {
    pub id: usize,
    pub name: String,
    pub description: String,
    pub is_direct: bool,
    pub image_id: Option<usize>,
    pub num_messages: usize,
    pub created_at: DateTime<Utc>,
    pub creator_id: usize,
}

#[derive(Clone, Debug)]
pub struct Image {
    pub id: Option<usize>,
    pub url_id: String,
    pub format: String,
    pub width: usize,
    pub height: usize,
    pub image: Option<Bytes>,
}

#[derive(Clone, Debug)]
pub struct User {
    pub id: usize,
    pub name: String,
    pub pfp_id: Option<usize>,
}

#[derive(Clone, Debug)]
pub struct Nickname {
    pub user_id: usize,
    pub group_id: usize,
    pub nickname: String,
    pub pfp_id: usize,
}

#[derive(Clone, Debug)]
pub struct Membership {
    pub user_id: usize,
    pub chat_id: usize,
}

impl Chat {
    pub fn set_image_id(&mut self, id: usize) {
        self.image_id = Some(id);
    }
}
impl From<api::ChatDetail> for Chat {
    fn from(chat: api::ChatDetail) -> Self {
        let created_at = NaiveDateTime::from_timestamp_opt(chat.created_at as i64, 0)
            .expect("GroupMe timekeeping");
        let created_at = Utc.from_local_datetime(&created_at).unwrap();

        return Self {
            id: chat.id.parse().expect("GroupMe group IDs are numbers"),
            name: chat.name,
            description: chat.description,
            is_direct: false,
            image_id: None,
            num_messages: chat.messages.count,
            created_at,
            creator_id: chat.creator_user_id.parse().expect("GroupMe user ID"),
        };
    }
}

impl Image {
    pub fn get_id(&self, db_conn: &db::Connection) -> Result<RowID> {
        let mut stmt = db_conn
            .prepare_cached(
                r#"
                SELECT id
                FROM images
                WHERE url_id = ?;
                "#,
            )
            .expect("Hard coded SQL SELECT statement");
        return Ok(stmt
            .query_map([&self.url_id], |row| {
                Ok(row.get(0).expect("Only querying ID 1"))
            })
            .context("Unable to query image table")?
            .next()
            .with_context(|| format!("No image with url_id = '{}'", self.url_id))?
            .expect("Only querying ID 2"));
    }
}
impl From<Url> for Image {
    fn from(url: Url) -> Self {
        // Format: https://i.groupme.com/{width}x{height}.{format}.{id}
        lazy_static! {
            static ref RE: Regex = Regex::new(
                r"(?P<width>\d+)x(?P<height>\d+)\.(?P<format>[a-z]+)\.(?P<url_id>[a-z0-9]+)"
            )
            .expect("Hardcoded regex");
            // Some URLS are malformed
            static ref ERROR_SINGLETON: Image = Image {
                id: Some(1),
                url_id: String::from("invalid"),
                format: String::from("ERR"),
                width: 0,
                height: 0,
                image: None,
            };
            // SMS users have profile image: https://i.groupme.com/sms_avatar
            static ref SMS_SINGLETON: Image = Image {
                id: Some(2),
                url_id: String::from("sms_singleton"),
                format: String::from("SMS"),
                width: 0,
                height: 0,
                image: None,
            };
            // Stock pfp or group images don't have a URL.
            static ref STOCK_IMAGE: Image = Image {
                id: Some(3),
                url_id: String::from("stock_image"),
                format: String::from("STCK"),
                width: 0,
                height: 0,
                image: None,
            };
        }
        match url.as_str() {
            "https://i.groupme.com/sms_avatar" => return SMS_SINGLETON.clone(),
            "file://" => return STOCK_IMAGE.clone(),
            _ => (),
        }

        let matches = match RE
            .captures_iter(
                url.path_segments()
                    .expect("Well defined image URL")
                    .rev()
                    .next()
                    .expect("Well defined image URL"),
            )
            .next()
        {
            Some(m) => m,
            None => return ERROR_SINGLETON.clone(),
        };

        return Self {
            id: None,
            url_id: matches["url_id"].to_string(),
            format: matches["format"].to_string(),
            width: matches["width"].parse().unwrap(),
            height: matches["height"].parse().unwrap(),
            image: None,
        };
    }
}

impl User {
    pub fn set_pfp_id(&mut self, id: usize) {
        self.pfp_id = Some(id);
    }
}
impl From<api::UserDetail> for User {
    fn from(user: api::UserDetail) -> Self {
        Self {
            id: user.user_id.parse().expect("GroupMe user IDs are numbers"),
            name: user.nickname,
            pfp_id: None,
        }
    }
}

impl Nickname {
    pub fn new(user_id: usize, group_id: usize, nickname: String, pfp_id: usize) -> Self {
        Self {
            user_id,
            group_id,
            nickname,
            pfp_id,
        }
    }
}

impl Membership {
    pub fn new(user_id: usize, chat_id: usize) -> Self {
        Self { user_id, chat_id }
    }
}
