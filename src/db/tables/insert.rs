use anyhow::{Context, Result};
use rusqlite::Statement;

use crate::db::tables::*;
use crate::db::DB;

pub trait Insertable {
    fn insert(&self, stmt: &mut Statement<'_>) -> Result<RowID>;
}

trait ConvertRowID {
    fn convert_rowid(self, table: &str) -> Result<RowID>;
}

impl ConvertRowID for db::Result<i64> {
    fn convert_rowid(self, table: &str) -> Result<RowID> {
        self.map(|i| i as RowID)
            .with_context(|| format!("Unable to insert {table}"))
    }
}

impl Insertable for Image {
    fn insert(&self, stmt: &mut Statement<'_>) -> Result<RowID> {
        stmt.insert(db::params![
            self.url_id,
            self.format,
            self.width,
            self.height
        ])
        .convert_rowid("image")
    }
}

impl Insertable for User {
    fn insert(&self, stmt: &mut Statement<'_>) -> Result<RowID> {
        stmt.insert(db::params![
            self.id,
            self.name,
            self.pfp_id
                .context("User does not have profile picture ID")?
        ])
        .convert_rowid("user")
    }
}

impl Insertable for Chat {
    fn insert(&self, stmt: &mut Statement<'_>) -> Result<RowID> {
        stmt.insert(db::params![
            self.id,
            self.name,
            self.description,
            self.is_direct,
            self.image_id.context("Chat does not have image ID")?,
            self.num_messages,
            self.created_at,
            self.creator_id
        ])
        .convert_rowid("chat")
    }
}

impl Insertable for Membership {
    fn insert(&self, stmt: &mut Statement<'_>) -> Result<RowID> {
        stmt.insert(db::params![self.user_id, self.chat_id])
            .convert_rowid("membership")
    }
}
