use std::{fs, io, path::PathBuf};

use anyhow::{Context, Result};
use lazy_static::lazy_static;
use rusqlite::{params, Connection, Error, Statement};
use url::Url;

mod tables;

use crate::db::tables::insert::Insertable;
use crate::groupme::api;
use tables::*;

pub type RowID = usize;

pub struct DB {
    file: PathBuf,
    connection: Connection,
}

impl DB {
    pub fn init(file: PathBuf) -> Result<Self> {
        if let Err(e) = fs::remove_file(&file) {
            if e.kind() != io::ErrorKind::NotFound {
                return Err(e).with_context(|| format!("Unable to delete database file: {file:?}"));
            }
        }

        let connection = Connection::open(&file).context("Unable to open SQLite connection")?;

        connection
            .execute_batch(include_str!("archive.sql"))
            .context("Unable to initialize database schema")?;

        return Ok(DB { file, connection });
    }

    pub fn load_chats(&self, chats: Vec<api::ChatDetail>) -> Result<()> {
        lazy_static! {
            static ref STOCK_IMAGE_URL: Url = Url::parse("file://").expect("Hardcoded url");
        }
        for chat in chats {
            let image = tables::Image::from(
                chat.image_url
                    .clone()
                    .unwrap_or_else(|| STOCK_IMAGE_URL.clone()),
            );
            let members = chat.members.clone();
            let mut chat = tables::Chat::from(chat);

            let chat_image_id = self.insert_media(image)?;
            chat.set_image_id(chat_image_id);

            let chat_id = self.insert_chat(chat)?;

            for mut member in members {
                let url = match Url::parse(&member.image_url) {
                    Ok(u) => u,
                    Err(_) => continue,
                };
                let image = tables::Image::from(url);
                let pfp_id = self.insert_media(image)?;

                let mut user = tables::User::from(member);
                user.set_pfp_id(pfp_id);

                let user_id = self.insert_user(user)?;

                let membership = Membership::new(user_id, chat_id);
                self.insert_membership(membership)?;
            }
        }
        return Ok(());
    }

    pub fn query(&self, query: &str) -> Result<Statement<'_>> {
        self.connection
            .prepare(query)
            .context("Unable to prepare SQL statement")
    }

    fn insert_chat(&self, group: Chat) -> Result<RowID> {
        let mut stmt = self
            .connection
            .prepare(
                r#"
                INSERT INTO chats
                VALUES (?, ?, ?,  ?, ?, ?, ?, ?);
                "#,
            )
            .expect("Hard coded SQL INSERT statement");
        group.insert(&mut stmt)
    }

    fn insert_media(&self, media: Image) -> Result<RowID> {
        let mut stmt = self
            .connection
            .prepare_cached(
                r#"
                INSERT INTO images (url_id, format, width, height, image)
                VALUES (?, ?, ?, ?, NULL)
                ON CONFLICT DO NOTHING;
                "#,
            )
            .expect("Hard coded SQL INSERT statement");

        let result = media.insert(&mut stmt);
        if inserted_0_rows(&result) {
            return media.get_id(&self.connection);
        }
        return result;
    }

    fn insert_user(&self, user: User) -> Result<RowID> {
        let mut stmt = self
            .connection
            .prepare_cached(
                r#"
                INSERT INTO users (id, name, pfp_id)
                VALUES (?, ?, ?)
                ON CONFLICT DO NOTHING;
                "#,
            )
            .expect("Hard coded SQL INSERT statement");
        let result = user.insert(&mut stmt);
        if inserted_0_rows(&result) {
            return Ok(user.id);
        }
        return result;
    }

    fn insert_membership(&self, membership: Membership) -> Result<()> {
        let mut stmt = self
            .connection
            .prepare_cached(
                r#"
                INSERT INTO memberships
                VALUES (?, ?);
                "#,
            )
            .expect("Hard coded SQL INSERT statement");
        membership.insert(&mut stmt).map(|id| ())
    }
}

fn inserted_0_rows(result: &Result<RowID>) -> bool {
    match result.as_ref().map_err(|e| e.downcast_ref::<Error>()) {
        Err(Some(Error::StatementChangedRows(0))) => true,
        _ => false,
    }
}
