-- SQLite3 version = 3.41

-- Optimizations
PRAGMA synchronous = OFF;
PRAGMA journal_mode = MEMORY;

-- Stores both group and direct message chats
CREATE TABLE chats (
    id           INTEGER      PRIMARY KEY, -- GroupMe chat/user id
    name         VARCHAR(140) NOT NULL,
    description  VARCHAR(255) NOT NULL,
    is_direct    BOOLEAN      NOT NULL CHECK (is_direct IN (FALSE, TRUE)),
    image_id     INTEGER      NOT NULL REFERENCES images(id),
    num_messages INT          NOT NULL CHECK (num_messages >= 0),
    created_at   DATETIME     NOT NULL,
    creator_id   INTEGER      NOT NULL REFERENCES users(id),

    UNIQUE (id, is_direct) -- We use either a group chat's id or a user's id if its a direct chat
);

CREATE TABLE images (
    id       INTEGER    PRIMARY KEY,
    url_id   TEXT       NOT NULL UNIQUE,
    format   VARCHAR(4) NOT NULL,
    width    INT        NOT NULL CHECK(width >= 0),
    height   INT        NOT NULL CHECK(height >= 0),
    image    BLOB,

    url      TEXT       GENERATED ALWAYS AS (
                           format('https://i.groupme.com/%ux%u.%s.%s',
                               width,
                               height,
                               format,
                               url_id
                        )) VIRTUAL
);

CREATE TABLE users (
    id     INTEGER PRIMARY KEY,
    name   TEXT    NOT NULL,
    pfp_id INTEGER NOT NULL REFERENCES images(id)
);

CREATE TABLE nicknames (
    user_id  INTEGER     REFERENCES users(id),
    group_id INTEGER     REFERENCES users(id),
    nickname VARCHAR(50) NOT NULL,
    pfp_id   INTEGER     REFERENCES images(id),

    UNIQUE (user_id, group_id, nickname, pfp_id)
);

CREATE TABLE memberships (
    user_id INTEGER REFERENCES users(id),
    chat_id INTEGER REFERENCES chats(id),

    UNIQUE (user_id, chat_id)
);

-- "Messages" that show up in Group/DMs, determined by type
CREATE TABLE chat_transactions (
    id        INTEGER PRIMARY KEY,                   -- GroupMe Message id
    group_id  INT     NOT NULL REFERENCES chats(id),
    user_id   INT              REFERENCES users(id), -- Nullable to allow non-user related status messages (rename, group info change, etc)
    type      INT     NOT NULL CHECK (type >= 0),
        -- Enum (unsigned):
        --  0: System message
    data      TEXT    NOT NULL
        -- JSON:
        -- System message:
        --   data['system_type'] (enum, unsigned):
        --     0: Unknown (contents in data['raw'])
        --     1: User(_id) joined
        --     2: User(_id) left
        --     3: User(_id) renamed (to data['name'])
        --     4: User(_id) changed avatar (to data['avatar'])
        --     5: User(_id) added new user (data['new_user_id'])
);

INSERT INTO images (id, url_id, format, width, height, image)
VALUES
    (1, 'invalid', 'ERR', 0, 0, NULL),
    (2, 'sms_singleton', 'SMS', 0, 0, NULL),
    (3, 'blank_image', 'STCK', 0, 0, NULL);