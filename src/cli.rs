use std::ffi::OsStr;
use std::path::PathBuf;

use clap::Parser;

#[derive(Parser)]
#[command(author, version, about)]
/// Downloads all GroupMe chats/images/profiles via an API token
pub struct CLIParams {
    /// GroupMe API token file, if not given or '-' then reads from stdin.
    #[arg(short, long)]
    pub token_file: Option<PathBuf>,

    /// Suppress all progress/output.
    #[arg(short, long)]
    pub quiet: bool,

    /// SQLite file to write to, overwrites if exists.
    pub db_file: PathBuf,
}

pub fn get_args() -> CLIParams {
    let mut args = CLIParams::parse();

    args.token_file = args
        .token_file
        .filter(|file| file.as_os_str() != "-")
        .or_else(|| Some(PathBuf::from("/dev/stdin")));

    return args;
}
