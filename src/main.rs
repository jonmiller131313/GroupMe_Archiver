use std::{
    fs::File,
    io::{BufRead, BufReader},
    mem::size_of,
    path::{Path, PathBuf},
};

use anyhow::{Context, Error, Result};
use lazy_static::lazy_static;
use reqwest::{blocking::Client, Error as ReqError};
use rusqlite::{params, Error as DBError};
use url::Url;

mod cli;
mod db;
mod groupme;

use db::RowID;
use groupme::api::Message;
use groupme::{DownloadFromGroupMe, Token};

fn main() -> Result<()> {
    let args = cli::get_args();
    let token_file = args
        .token_file
        .expect("Arg parsing always sets the token file");
    let db = db::DB::init(args.db_file).context("Unable to initialize database")?;

    let groupme_connection =
        Client::try_from(Token::read(&token_file, !args.quiet).context("Unable to read token")?)
            .with_context(|| format!("Unable to read token file: {:?}", &token_file))?;

    let groups = groupme_connection
        .get_groups()
        .context("Unable to get groups")?;

    db.load_chats(groups).context("Unable to write groups")?;

    let chat_details: Vec<(RowID, String, usize)> = db
        .query(
            r#"
            SELECT id, name, num_messages
            FROM chats;
            "#,
        )
        .context("Unable to prepare chats query to load messages")?
        .query_map(params![], |row| {
            Ok((row.get_unwrap(0), row.get_unwrap(1), row.get_unwrap(2)))
        })
        .context("Unable to query chat detail rows")?
        .collect::<Result<_, _>>()
        .context("Unable to get chat details to load messages")?;
    let messages = groupme_connection
        .get_all_messages(&chat_details, !args.quiet)
        .context("Unable to download messages for chats")?;

    let mem_size = messages.iter().flat_map(|messages| messages.iter()).count();

    println!(
        "Size of message in memory: {}",
        mem_size * size_of::<Message>()
    );

    return Ok(());
}
